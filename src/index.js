$( document ).ready(function() {
  $( "#toggle-menu" ).click(function() {
    if ($("#sidebar").hasClass("close")){
      activeDesktop();
    } else {
      activeMobile();
    }
  });
  
  mobileNav();

  

});

$(window).on('resize', function(){
  mobileNav();
});

function activeDesktop(){
  
  $("#sidebar").removeClass("close")
  $("#wrapper").removeClass("full")
  $("#toggle-menu-icon").attr("class", "fa-angle-left fas")
  
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    var toolElement = bootstrap.Tooltip.getInstance(tooltipTriggerEl)
    if (toolElement) {toolElement.disable()}
  })

}

function activeMobile(){
  
  $("#sidebar").addClass("close")
  $("#wrapper").addClass("full")
  $("#toggle-menu-icon").attr("class", "fa-angle-right fas")
  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]')) 
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
  })

}

function mobileNav() {
  var width = $(window).width();
  if (width > 767) {
    activeDesktop();
  } else {
    activeMobile();
  }
}



const ctx = document.getElementById("chart").getContext('2d');
const lineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: ["Sunday", "Monday", "Tuesday",
    "Wednesday", "Thursday", "Friday", "Saturday"],
    datasets: [{
      label: 'Last week',
      backgroundColor: '#0560fd',
      borderColor: '#ced4da',
      borderWidth: 0.5,
      data: [3000, 4000, 2000, 5000, 8000, 9000, 2000],
    }]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
        }
      }]
    }
  },
});

const ctx_bar = document.getElementById("chart_bar").getContext('2d');
const barChar = new Chart(ctx_bar, {
  type: 'pie',
  data: {
    labels: ["Sunday", "Monday", "Tuesday",
    "Wednesday", "Thursday", "Friday", "Saturday"],
    datasets: [{
      label: 'Last week',
      backgroundColor: '#0560fd',
      borderColor: '#ced4da',
      borderWidth: 0.5,
      data: [3000, 4000, 2000, 5000, 8000, 9000, 2000],
    }]
  },
});


const ctx_radar = document.getElementById("chart_radar").getContext('2d');
const barRadar = new Chart(ctx_radar, {
  type: 'radar',
  data: {
    labels: ["Sunday", "Monday", "Tuesday",
    "Wednesday", "Thursday", "Friday", "Saturday"],
    datasets: [{
      label: 'Last week',
      backgroundColor: '#0560fd',
      borderColor: '#ced4da',
      borderWidth: 0.5,
      data: [3000, 4000, 2000, 5000, 8000, 9000, 2000],
    }]
  },
});

const ctx_polar_area = document.getElementById("chart_polar_area").getContext('2d');
const barPolarArea = new Chart(ctx_polar_area, {
  type: 'polarArea',
  data: {
    labels: ["Sunday", "Monday", "Tuesday",
    "Wednesday", "Thursday", "Friday", "Saturday"],
    datasets: [{
      label: 'Last week',
      backgroundColor: '#0560fd',
      borderColor: '#ced4da',
      borderWidth: 0.5,
      data: [3000, 4000, 2000, 5000, 8000, 9000, 2000],
    }]
  },
});